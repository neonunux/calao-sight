export function getLocale() {
  return 'fr';
}

export function setTitle(title) {
  if (title) {
    document.title = `${title} | Calao`;
  } else {
    document.title = 'Calao';
  }
}

export function csrfToken() {
  return document.getElementById('csrf_token').getAttribute('content');
}

export function gaEvent(category, action, label, value, interactive = true) {
  const opts = {
    hitType: 'event',
    eventCategory: category,
    eventAction: action,
    eventLabel: label,
    eventValue: value,
  };

  if (!interactive) {
    opts.nonInteraction = 1;
  }

  try {
    if (window.ga) {
      const { ga } = window;
      ga('send', opts);
    } else {
      // console.debug('event:', opts)
    }
  } catch (e) { }
}

window.N = window.N || {};
export const { N } = window;
