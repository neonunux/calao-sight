export function storageAvailable(type) {
  try {
    const storage = window[type];
    const x = '__test';
    storage.setItem(x, x);
    storage.removeItem(x);
    return true;
  } catch (e) {
    return false;
  }
}

export function writeConfig(name, value) {
  if (storageAvailable('localStorage')) {
    if (value === undefined) {
      console.info('Removing config', name);
      return window.localStorage.removeItem(name);
    }

    if (typeof value !== 'string') {
      value = JSON.stringify(value);
    }

    console.info('Writing config', name, value);
    return window.localStorage.setItem(name, value);
  }
}

export function readConfig(name, defaultValue = undefined) {
  if (storageAvailable('localStorage')) {
    console.info('Reading config', name);
    let ret = window.localStorage.getItem(name);
    if (ret === undefined) {
      ret = defaultValue;
    } else {
      try {
        ret = JSON.parse(ret);
      } catch (e) { }
    }

    return ret;
  }
  return defaultValue;
}
