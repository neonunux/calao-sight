import React from 'react';
import { NOTE_EVENTS } from '../../uncategorized/midi';
// import types from 'prop-types'

export default class LatencyPage extends React.Component {
  onMidiMessage(message) {
    if (!this.state.metronome) {
      return;
    }

    const [raw, velocity] = message.data;

    const // cmd = raw >> 4,
      // channel = raw & 0xf,
      type = raw & 0xf0;

    if (NOTE_EVENTS[type] === 'noteOn') {
      if (velocity !== 0) {
        console.log(this.state.metronome.getLatency());
      }
    }
  }

  render() {
    const metronomeButton = (
      <button
        onClick={(e) => {
          e.preventDefault();
          const metronome = this.props.midiOutput.getMetronome();
          this.setState({ metronome });
          metronome.start(60);
        }}
      >
        Start metronome
      </button>
    );

    return (
      <div className="latency_page">
        {this.props.midiOutput
          ? metronomeButton
          : 'Configure output to test latency'}
      </div>
    );
  }
}
