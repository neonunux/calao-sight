import classNames from 'classnames'
import React from 'react'
import ReactDOM from 'react-dom'
import { Link, NavLink } from 'react-router-dom'
import i18n from "i18next";

import { trigger } from '../../uncategorized/events'
import { N } from '../../uncategorized/globals'
import { IconDownArrow } from './icons'
import MidiButton from './midiButton'

import '../../scss/main.scss'

class SizedElement extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    this.refreshWidth()

    let timeout = null
    this.resizeCallback = e => {
      if (timeout) {
        window.clearTimeout(timeout)
        timeout = null
      }

      timeout = window.setTimeout(() => {
        this.refreshWidth()
        timeout = null
      }, 100)
    }

    window.addEventListener('resize', this.resizeCallback)
    this.resizeCallback()
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeCallback)
  }

  refreshWidth() {
    let el = ReactDOM.findDOMNode(this)
    let width = el.getBoundingClientRect().width
    if (this.state.width !== width) {
      this.setState(
        { width },
        function () {
          if (this.props.onWidth) {
            this.props.onWidth(this.state.width)
          }
        },
      )
    }
  }

  render() {
    return (
      <div className={classNames('sized_element', this.props.className)}>
        {this.state.width ? this.props.children : null}
      </div>
    )
  }
}

export default class Header extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      menuOpen: false,
    }
  }

  renderNavigationMenu() {
    const userLinks = this.getPageLinks()
    const showMidiButton = !this.state.width || this.state.width < 450
    let menu = null

    if (this.state.menuOpen) {
      let account_area = null
      if (N.session.currentUser) {
        account_area = (
          <div className="account_area logged_in">
            <span className="username">{N.session.currentUser.username}</span>
            <a href="/" onClick={this.props.doLogout}>
              {i18n.t('header.log-out')}
            </a>
          </div>
        )
      } else {
        account_area = (
          <div className="account_area logged_out">
            <NavLink to="/login" activeClassName="active">
              {i18n.t('header.log-in')}
            </NavLink>
            <NavLink to="/register" activeClassName="active">
              {i18n.t('header.register')}
            </NavLink>
          </div>
        )
      }

      menu = (
        <div
          key="navigation_menu"
          ref={el => {
            if (el) {
              el.focus()
            }
          }}
          onClick={e => {
            if (e.target.matches('a')) {
              this.setState({ menuOpen: false })
            }
          }}
          className="navigation_menu"
          tabIndex="-1"
        >
          {account_area}
          {showMidiButton ? (
            <div className="midi_button_wrapper">{this.renderMidiButton()}</div>
          ) : null}
          <ul>
            {userLinks.map((link, i) => (
              <li key={i}>{link}</li>
            ))}
          </ul>
        </div>
      )
    }

    return (
      <div className="menu_toggle">
        <button
          type="button"
          onClick={e => {
            this.setState({ menuOpen: !this.state.menuOpen })
          }}
        >
          Menu {<IconDownArrow width={12} />}
        </button>
        {menu ? (
          <div
            onClick={e => this.setState({ menuOpen: false })}
            className="menu_shroud"
          ></div>
        ) : null}
        {menu}
      </div>
    )
  }

  renderHorizontalNavigation() {
    let userPanel = null
    let userLinks = this.getPageLinks()

    if (N.session.currentUser) {
      userPanel = (
        <div className="right_section" key="user_in">
          {N.session.currentUser.username}{' '}
          <a href="/" onClick={this.props.doLogout}>
            {i18n.t('header.log-out')}
          </a>
        </div>
      )
    } else {
      userPanel = (
        <div className="right_section" key="user_out">
          <NavLink to="/login" activeClassName="active">
            {i18n.t('header.log-in')}
          </NavLink>
          {i18n.t('header.or')}
          <NavLink to="/register" activeClassName="active">
            {i18n.t('header.register')}
          </NavLink>
        </div>
      )
    }

    return [...userLinks, userPanel]
  }

  getPageLinks() {
    let links = [
      <NavLink
        exact
        key="root"
        to="/"
        activeClassName="active"
      >
        <img src="../img/eye.svg" alt={i18n.t('header.eye')} />
      </NavLink>,
      <NavLink
        exact
        key="ear-training"
        to="/ear-training/interval-melodies"
        activeClassName="active"
      >
        <img src="../img/ear.svg" alt={i18n.t('header.ear')} />
      </NavLink>,
      <NavLink
        exact
        key="flash-cards"
        to="/flash-cards/note-math"
        activeClassName="active"
      >
        <img src="../img/brain.svg" alt={i18n.t('header.brain')} />
      </NavLink>,
      <NavLink
        exact
        key="play-along"
        to="/play-along"
        activeClassName="active">
        <img src="../img/arcade.svg" alt={i18n.t('header.play-along')} />

      </NavLink>,
      <NavLink
        exact
        key="about"
        to="/about"
        activeClassName="active">
        <img src="../img/guide.svg" alt={i18n.t('header.guide')} />

      </NavLink>,
      <NavLink
        exact
        key="fine-tuning"
        to="/fine-tuning"
        activeClassName="active">
        <img src="../img/guide.svg" alt={i18n.t('header.guide')} />

      </NavLink>,
    ]

    if (N.session.currentUser) {
      links.push(
        <NavLink exact key="stats" to="/stats" activeClassName="active">
          {i18n.t('header.stats')}
        </NavLink>,
      )
    }
    return links
  }

  renderMidiButton() {
    return (
      <MidiButton
        midiInput={this.props.midiInput}
        pickMidi={() => {
          trigger(this, 'pickMidi')
        }}
      />
    )
  }

  render() {
    // let userPanel = null
    let enableDropdown = this.state.width && this.state.width < 700
    let hideMidiButton = !this.state.width || this.state.width < 450

    return (
      <div className="header">
        <Link to="/" className="logo_link">
          <img className="logo" src="../img/logo.svg" height="35" alt="" />
          <img
            className="logo_small"
            src="../img/logo-small.svg"
            height="35"
            alt=""
          />
        </Link>

        <SizedElement
          className="user_links"
          onWidth={w => {
            this.setState({ width: w })
          }}
        >
          {enableDropdown
            ? this.renderNavigationMenu()
            : this.renderHorizontalNavigation()}
        </SizedElement>

        {hideMidiButton ? null : this.renderMidiButton()}
      </div>
    )
  }
}
