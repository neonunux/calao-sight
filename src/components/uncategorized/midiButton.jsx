import React from 'react'
import i18n from "i18next";

export default function MidiButton(props) {
  return <button
    onClick={(e) => {
      e.preventDefault()
      props.pickMidi()
    }}
    className="midi_button">
    <div>
      <img src="../svg/midi.svg" alt="MIDI" />
      <span className="current_input_name">
        {props.midiInput ? props.midiInput.name : i18n.t('midi.select-device')}
      </span>
    </div>
  </button>
}

