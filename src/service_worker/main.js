
const CACHE_VERSION = 'v4';
const CACHE_NAME = 'st_cache';

const urlsToCache = ['/'];

window.self.addEventListener('install', (event) => {
  event.waitUntil(caches.open(`${CACHE_VERSION}:${CACHE_NAME}`).then((cache) => cache.addAll(urlsToCache)));
});

window.self.addEventListener('fetch', (event) => {
  if (event.request.method !== 'GET') {
    return;
  }

  if (event.request.url.match(/\bmanifest\.json\b/)) {
    return;
  }

  event.respondWith(
    caches.open(`${CACHE_VERSION}:${CACHE_NAME}`).then((cache) => fetch(event.request).then((response) => {
      cache.put(event.request, response.clone());
      return response;
    }).catch(() => cache.match(event.request))),
  );
});

window.self.addEventListener('activate', (event) => {
  event.waitUntil(caches.keys().then((keys) => Promise.all(keys
    .filter((key) => !key.startsWith(CACHE_VERSION))
    .map((key) => caches.delete(key)))));
});
