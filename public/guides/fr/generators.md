# Note Generators

Afin de fournir des partitions uniques à lire, Sight Reading Trainer utilise un générateur de musique aléatoire. Vous pouvez personnaliser le générateur pour contrôler la difficulté et les types de choses que vous souhaitez pratiquer.

Accédez aux paramètres du générateur en cliquant sur le bouton **Configurer** de l'écran principal.

## Choisir un staff

Le personnel que vous choisissez configure la gamme de notes pouvant être lues. Le clavier à l'écran n'affiche que les notes valides pouvant être jouées. Toutes les notes générées tomberont dans la gamme du personnel choisi.

Vous pouvez choisir parmi les options suivantes:

* <img src="/svg/clefs.G.svg" alt="G Cleff" width="25" height="25" /> Treble
* <img src="/svg/clefs.F_change.svg" alt="F Cleff" width="25" height="20" /> Basse
* Grand - Une combinaison des aigus et des basses en même temps

## Choisir un type de générateur

Sight Reading Trainer essaie de générer quelque chose de musical basé sur les paramètres que vous avez fournis. Le type de générateur ** ** est la fonction qui sélectionne les notes à afficher ensuite. Chaque type de générateur peut être personnalisé à l'aide d'une série de paramètres.

Les types de générateurs disponibles :

* **Aléatoire** - Choisit 1 à 5 notes aléatoires dans la clé choisie pour être jouées dans chaque colonne
* **Triades** - Choisit une [triade](https://fr.wikipedia.org/wiki/Accord_de_trois_notes) de notes dans une inversion aléatoire en voix rapprochée dans la signature de clé.
* **Sevens** - Choisit un [septième accord](https://fr.wikipedia.org/wiki/Accord_de_septi%C3%A8me_d%27esp%C3%A8ce) aléatoire  avec une voix ouverte (celle-ci est la plus agréable)
* **Progression** - Choisit un accord aléatoire parmi une progression populaire dans la signature de clé
* **Position** - Génère des notes de manière à vous encourager à utiliser tous vos doigts. Voir ci-dessous pour plus d'informations

## Le paramètre de finesse

Chaque générateur a un paramètre **de douceur**. Le paramètre **lissage** rend le caractère aléatoire moins apparent en minimisant les mouvements de notes pour chaque colonne des notes générées. (Cependant, il n'est jamais possible de répéter les mêmes notes).

S'il y a plusieurs notes, cela minimisera la position moyenne des notes dans la colonne.

Plus le réglage est élevé, plus le générateur effectuera d'itérations pour trouver un prochain jeu de notes, plus les mouvements seront lisses.

## Le générateur aléatoire

Les paramètres suivants sont disponibles pour le générateur **Random**:

* **Notes** - Combien de notes à générer à la fois
* **Mains** - Combien de mains doivent être utilisées pour jouer toutes les notes. Par exemple, si vous voulez vous entraîner à jouer 5 notes d’accord dans une main, définissez les notes sur 5 et les mains sur 1.
* **Basé sur les accords** - La colonne de notes sera limitée aux notes pouvant être formées à partir de tiers empilés

### Le générateur de position

Le ** générateur de position ** est conçu pour que vous utilisiez tous vos doigts lors de la lecture à vue. Vous recevrez des notes par lot de 5, la première contenant un doigté. Toutes les notes suivantes doivent être jouées sans bouger votre main et avec chacun de vos doigts.
