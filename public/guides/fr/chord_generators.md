# Mode d'accord

En plus des notes de lecture à vue sur les partitions, vous pouvez utiliser le personnel des accords pour lire les accords par nom. Les accords sont générés dans la tonalité souhaitée et vous devez appuyer sur le nombre minimum de notes pour continuer jusqu'à l'accord suivant.

Vous pouvez accéder au mode d'accord en allant sur **Staff**, en sélectionnant **Configure**, puis **Chord** dans l'option *Staff*. Si vous souhaitez générer des accords en dehors d'une clé spécifique, vous pouvez choisir le mode **Chromatique** sous *Clé*.

Voici les autres options configurables:

* **notes** - Le nombre de notes de chaque accord, vous permettant de choisir entre trois triades ou sept accords.
* **notes communes** - Le nombre de notes que l'accord suivant doit partager avec l'accord précédent. Cela peut rendre les accords plus sains

## Jouer un accord

Pour réussir à jouer un accord, vous devez au moins jouer toutes les notes qu'il contient. Vous pouvez jouer les accords dans la voix de votre choix, en répétant un nombre quelconque de notes. L'ordre dans lequel les notes sont jouées n'a pas d'importance. Vous pouvez l'utiliser pour créer des défis supplémentaires pour vous-même:

* Jouer dans une inversion spécifique - Mettez-vous à l'aise en jouant tous les accords dans une inversion spécifique
* Minimiser les mouvements - Essayez de minimiser le nombre de mouvements de votre main en choisissant une inversion proche de l'accord que vous venez de jouer.
* Jouer avec les deux mains - Apprenez les accords avec vos deux mains. Vous pouvez essayer d’utiliser la même voix ou une voix différente pour chaque main.